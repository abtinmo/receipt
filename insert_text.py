from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.pdfgen import canvas
import io
import uuid
import jalal
from arabic_reshaper import ArabicReshaper
from bidi.algorithm import get_display
import persian
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont


def spbycoma(string):
    """
    seperate numbers with coma
    123456 ==> 123,456
    """
    string=string[::-1]
    li = [string[i:i+3] for i in range(0, len(string), 3)][::-1]
    string = ','.join(li)
    return persian.convert_en_numbers(string)


def convert_to_jalali(data):
    return persian.convert_en_numbers(jalal.Gregorian(data).persian_string())


def make_rtl(string):
    configuration = {'language':'Farsi'}
    reshaper = ArabicReshaper(configuration=configuration)
    return get_display(reshaper.reshape(string))


def editPdf(payload, name=uuid.uuid4().hex + ".pdf"):
    pdfmetrics.registerFont(TTFont('Shabnam', 'Shabnam.ttf'))
    # conif arabic reshaper

    # meke new pdf
    packet = io.BytesIO()
    pdf = canvas.Canvas(packet)
    pdf.setFont("Shabnam", 14)
    pdf.setPageSize((716 , 449))

    # draw data on pdf
    pdf.drawRightString(614, 416, payload['order_id'])
    pdf.drawRightString(124, 416, convert_to_jalali(payload['order_date']))
    pdf.drawRightString(546, 356, make_rtl(payload['menu']))

    # add products
    pdf.drawRightString(630, 245, make_rtl(payload['row1']['desc1']))
    pdf.drawRightString(350, 245, make_rtl(payload['row1']['amount1']))
    pdf.drawRightString(225, 245, spbycoma(payload['row1']['price1']))
    pdf.drawRightString(130, 245, spbycoma(payload['row1']['total1']))
    pdf.drawRightString(630, 200, make_rtl(payload['row2']['desc2']))
    pdf.drawRightString(350, 200, make_rtl(payload['row2']['amount2']))
    pdf.drawRightString(225, 200, spbycoma(payload['row2']['price2']))
    pdf.drawRightString(130, 200, spbycoma(payload['row2']['total2']))
    pdf.drawRightString(310, 146, spbycoma(payload['full_price']))
    pdf.drawRightString(636, 91, make_rtl(payload['desc']))
    pdf.save()



    # merg pdfs
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    ex_pdf = PdfFileReader(open("template.pdf","rb"))
    outpout = PdfFileWriter()
    page = ex_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    outpout.addPage(page)
    out_stream = open(name, "wb")
    outpout.write(out_stream)
    out_stream.close()




if __name__ == "__main__":
    payload = {
        'order_id':'12345678',
        'order_date':'2018-4-6',
        'menu':'از کوی معلم',
        'full_price':'132456846',
        'desc':'ضمانت تا ۳ ماه بعد از تاریخ فاکتور',
        'row1':{
                'desc1':'محصول اول',
                'amount1':'3',
                'price1':'45638',
                'total1':'1446238'
        },
        'row2':{
                'desc2':'محصول دوم',
                'amount2':'3',
                'price2':'45638',
                'total2':'12346756'
        }
    }
    editPdf(payload)


